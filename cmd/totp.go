package main

import (
	"bytes"
	"flag"
	"fmt"
	"image/png"
	"io/ioutil"
	"os"
	"time"

	"github.com/liyue201/goqr"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
)

func recognizeFile(path string) (string, error) {
	imgdata, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	img, err := png.Decode(bytes.NewReader(imgdata))
	if err != nil {
		return "", err
	}
	qrCodes, err := goqr.Recognize(img)
	if err != nil {
		return "", err
	}
	for _, qrCode := range qrCodes {
		return string(qrCode.Payload), nil
	}
	return "", nil
}

func display(key *otp.Key) {
	fmt.Printf("URL:          %s\n", key)
	fmt.Printf("Issuer:       %s\n", key.Issuer())
	fmt.Printf("Account Name: %s\n", key.AccountName())
	fmt.Printf("Secret:       %s\n", key.Secret())
	fmt.Println("--------------")
}

func generatePassCode(utf8string string) (string, error) {
	passcode, err := totp.GenerateCode(utf8string, time.Now())
	return passcode, err
}

var url string
var imagePath string
var showInfo bool

func init() {
	flag.StringVar(&url, "url", "", "pass on an otpauth:// url")
	flag.StringVar(&imagePath, "image_path", "", "pass on the path to an qr code image")
	flag.BoolVar(&showInfo, "v", false, "display otp info")
}

func main() {
	flag.Parse()

	if flag.NFlag() < 1 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	if imagePath != "" {
		var err error
		url, err = recognizeFile(imagePath)
		if err != nil {
			fmt.Printf("Recognize failed: %v\n", err)
			os.Exit(1)
		}
	}

	otpKey, err := otp.NewKeyFromURL(url)
	if err != nil {
		fmt.Printf("OTP new key from url error: %v\n", err)
	}
	if showInfo {
		display(otpKey)
	}

	oldPasscode := ""
	for {
		passcode, err := generatePassCode(otpKey.Secret())
		if err != nil {
			fmt.Printf("Generate Passcode error: %v\n", err)
			os.Exit(1)
		}
		if oldPasscode != passcode {
			fmt.Printf("Passcode:     %s\n", passcode)
			oldPasscode = passcode
		}
		time.Sleep(time.Second)
	}
}

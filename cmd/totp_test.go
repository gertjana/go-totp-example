package main

import (
	"testing"

	"github.com/pquerna/otp"
	"github.com/stretchr/testify/assert"
)

func TestOTP(t *testing.T) {
	image := "../test/qr-code.png"

	url, err := recognizeFile(image)
	if err != nil {
		assert.Fail(t, err.Error())
	}
	key, err := otp.NewKeyFromURL(url)
	if err != nil {
		assert.Fail(t, err.Error())
	}

	assert.Equal(t, key.Issuer(), "Example.com")
	assert.Equal(t, key.AccountName(), "alice@example.com")
	assert.Equal(t, key.Secret(), "A3BGX4IS6M4VJDDFLMJB7XPX3AQTPIZT")
}

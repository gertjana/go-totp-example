### Golang TOTP Example

Example of an application that can read secrets from an url or qr-code image
and generate OTP's (One Time Password's) for the secret.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gertjana/go-totp-example)](https://goreportcard.com/report/gitlab.com/gertjana/go-totp-example)
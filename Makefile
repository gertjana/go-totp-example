app = totp
dist_server = dist/$(app)

default: build

format:
	@go fmt *.go

build-linux: 
	@GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o $(dist_server) cmd/$(app).go

build: 
	@CGO_ENABLED=0 go build -o $(dist_server) cmd/$(app).go

run:
	@go run cmd/$(app).go

test:  build
	@go test -v cmd/*

develop:
	@fresh

update-deps:
	@go mod tidy
	@go mod vendor

clean:
	@go clean
	@rm -fv dist/*


module gitlab.com/gertjana/totp

go 1.16

require (
	github.com/liyue201/goqr v0.0.0-20200803022322-df443203d4ea
	github.com/pquerna/otp v1.3.0
	github.com/stretchr/testify v1.4.0
)
